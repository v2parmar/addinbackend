package main

import (
	"bitbucket.org/emailgistics/emailgisticscommon/src/config"
	"bitbucket.org/emailgistics/emailgisticscommon/src/db"
	"bitbucket.org/emailgistics/emailgisticscommon/src/keyvault"
	"bitbucket.org/emailgistics/emailgisticscommon/src/logger"
	"bitbucket.org/emailgistics/emailgisticscommon/src/response"
	"bitbucket.org/emailgistics/emailgisticscommon/src/router"
	"github.com/dgrijalva/jwt-go"

	//"bitbucket.org/emailgistics/emailgisticscommon/src/structs"
	"context"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"net/http"
	"time"
)

type addinComment struct {
	HashId string `json:"hashId" bson:"HashId"`
	TimePosted time.Time `json:"timePosted" bson:"TimePosted"`
	DisplayName string `json:"displayName" bson:"DisplayName"`
	EmailAddress string `json:"emailAddress" bson:"EmailAddress"`
	CommentText string `json:"commentText" bson:"CommentText"`
}

const AddInDatabaseName = "AddIn"
const MessageCommentsCollection = "MessageComments"

func init() {
	// endpoints -> copied from othe places for testing
	//router.AddGetFunction("/addin/tenants/{tenId}/users/{userAddr}", GetUserCustomers)
	//router.AddPostFunction("/addin/update", UpdateUserAvailability)

	// add-in comments endpoints
	router.AddPutFunction("/addin/addComment", AddComment)
	router.AddGetFunction("/addin/loadComments/{indexVal}", LoadComments)

	router.AddGetFunction("/addin/readcookie", ReadCookie)
	router.AddPostFunction("/addin/jwttoken", decodeAccessToken)

	config.LoadConfig(keyvault.DemoManagerKeyVaultName)
}


// TEMP testing if cookie is set
func ReadCookie(w http.ResponseWriter, r *http.Request) {
	logger.Println("[ReadCookie] ENTER")
	cookiesList := r.Cookies()

	for k, v := range r.Header {
		logger.Println("[ReadCookie] header: ", k, " | ", v)
	}

	for _, c := range cookiesList {
		logger.Println("[ReadCookie] cookie: ", c.Name, " | ", c.Value)
	}

	logger.Println("[ReadCookie] EXIT")
	response.JsonResponse(w, map[string]string{"message": "successfully read all cookies"}, http.StatusOK)
}



// --------------------------------------------------- TESTING CODE - TEMPORARY ---------------------------------------------------
type MyCustomClaims struct {
	Email  string `json:"email"`
	Name   string `json:"name"`
	TenId  string `json:"tid"`
	Id     string `json:"oid"`
	CustId string `json:"cid"`
	//Region string `json:"region"`
	jwt.StandardClaims
}

var (
	jwtKey         string
	defaultKeyFunc jwt.Keyfunc = func(t *jwt.Token) (interface{}, error) { return []byte(jwtKey), nil }
)

func decodeAccessToken(w http.ResponseWriter, r *http.Request) {
	reqObj := make(map[string]string)

	err1 := json.NewDecoder(r.Body).Decode(&reqObj)
	if err1 != nil {
		logger.Println("[decodeAccessToken] failed to decode request body")
		w.WriteHeader(400)
		return
	}

	token := reqObj["tokenVal"]
	logger.Println("token: ", token)
	parseWithClaims := false

	parser := jwt.Parser{}
	var tok *jwt.Token
	var err error

	methods := []string{}
	methods = append(methods, jwt.SigningMethodHS256.Alg())
	parser.ValidMethods = methods
	parser.SkipClaimsValidation = true

	if parseWithClaims {
		if jwtKey == "" {
			logger.Println("[decodeAccessToken] jwt key is empty string")
			w.WriteHeader(400)
			return
		}

		tok, err = parser.ParseWithClaims(token, &MyCustomClaims{}, defaultKeyFunc)
		if err != nil {
			logger.Println("Error decoding JWT: ", err)
			logger.Println("[decodeAccessToken] parse with claims failed")
			w.WriteHeader(400)
			return
		}

	} else {
		tok, _, err = parser.ParseUnverified(token, &MyCustomClaims{})
		if err != nil {
			logger.Println("Error parsing id token: ", err)
			logger.Println("[decodeAccessToken] parse unverified failed")
			w.WriteHeader(400)
			return
		}
	}

	var returnedclaims *MyCustomClaims

	if claims, ok := tok.Claims.(*MyCustomClaims); ok {
		logger.Println("Email: ", claims.Email)
		returnedclaims = claims
	} else {
		logger.Println("Error parsing token for user: ", err)
		logger.Println("Ok: ", ok)
	}

	x, _ := json.Marshal(returnedclaims)
	logger.Println(string(x))
	logger.Println("[decodeAccessToken] tok.Valid: ", tok.Valid)
	w.WriteHeader(200)
}
// --------------------------------------------------- END TESTING - TEMPORARY ---------------------------------------------------

// add a comment to an email
func AddComment(w http.ResponseWriter, r *http.Request) {
	var newComment addinComment

	// decode the request body into a addinComment struct
	err1 := json.NewDecoder(r.Body).Decode(&newComment)
	if err1 != nil {
		logger.Println("[AddComment] ERROR: failed to decode request to post new addin comment; ", err1.Error())
		//msgMarsh, _ := json.Marshal(map[string]string{"message": "failed to decode request to post new addin comment; " + err1.Error()})
		response.JsonResponse(w, map[string]string{"message": "failed to decode request to post new addin comment; " + err1.Error()}, http.StatusBadRequest)
		return
	}

	logger.Println("[AddComment] comment object: ", newComment)

	// hash the string provided in the request and set it as the ID
	hashArr := sha256.Sum256([]byte(newComment.HashId))
	newComment.HashId = hex.EncodeToString(hashArr[:])
	logger.Println("[AddComment] HashId: ", newComment.HashId)

	// configure database client
	// TODO: need to get customerClient later when I can pass customerId to this function
	client := db.GetClient(config.RegionalDatabaseName)
	if client == nil {
		logger.Println("[AddComment] ERROR: client is nil")
		//msgMarsh, _ := json.Marshal(map[string]string{"message": "client is nil"})
		response.JsonResponse(w, map[string]string{"message": "client is nil"}, http.StatusBadGateway)
		return
	}

	// insert the comment into the comments collection
	// TODO: switch to customerId database when possible
	commentsColl := client.Database(AddInDatabaseName).Collection(MessageCommentsCollection)
	_, err2 := commentsColl.InsertOne(context.TODO(), newComment)
	if err2 != nil {
		logger.Println("[AddComment] ERROR: failed to insert new comment into database; ", err2.Error())
		//msgMarsh, _ := json.Marshal(map[string]string{"message": "failed to insert new comment into database; " + err2.Error()})
		response.JsonResponse(w, map[string]string{"message": "failed to insert new comment into database; " + err2.Error()}, http.StatusBadGateway)
		return
	}

	// send the success response message
	//msgMarsh, _ := json.Marshal(map[string]string{"message": "addComment function completed successfully"})
	response.JsonResponse(w, map[string]string{"message": "addComment function completed successfully"}, http.StatusCreated)
}


// return all of the comments for the given InternetMessageId
func LoadComments(w http.ResponseWriter, r *http.Request) {
	// extract the index string from the URL
	vars := mux.Vars(r)
	indexVal := vars["indexVal"]
	logger.Println("indexVal: ", indexVal)

	// produce the hash value using the provided input string
	hashArr := sha256.Sum256([]byte(indexVal))
	hashId := hex.EncodeToString(hashArr[:])
	logger.Println("[LoadComments] HashId: ", hashId)

	// configure database client
	// TODO: need to get customerClient later when I can pass customerId to this function
	client := db.GetClient(config.RegionalDatabaseName)
	if client == nil {
		logger.Println("ERROR: client is nil")
		//msgMarsh, _ := json.Marshal(map[string]string{"message": "client is nil"})
		response.JsonResponse(w, map[string]string{"message": "client is nil"}, http.StatusBadGateway)
		return
	}

	// the array of comment objects to return
	var commentsList []addinComment


	// query the database for all comments with the given InternetMessageId
	// TODO: switch to customerId database when possible
	commentsColl := client.Database(AddInDatabaseName).Collection(MessageCommentsCollection)
	queryOptions := options.Find().SetSort(bson.D{{"TimePosted", 1}})
	commCur, err1 := commentsColl.Find(context.TODO(), bson.D{{"HashId", hashId}}, queryOptions)
	defer commCur.Close(context.TODO())

	// check if the Find query was successful
	if err1 != nil {
		logger.Println("[LoadComments] query to database failed; ", err1.Error())
		//msgMarsh, _ := json.Marshal(map[string]string{"message": "query to database failed; " + err1.Error()})
		response.JsonResponse(w, map[string]string{"message": "query to database failed; " + err1.Error()}, http.StatusBadRequest)
		return
	}

	// use the commCur cursor to loop through every document returned from the Find query
	for commCur.Next(context.TODO()) {
		var comm addinComment

		// decode the current document into an AddinComment struct
		decErr := commCur.Decode(&comm)
		if decErr != nil {
			logger.Println("[LoadComments] ERROR: could not decode comment object from database; ", decErr)
			continue
		} else {
			// if decode was successful, append the struct to the list of comments
			commentsList = append(commentsList, comm)
		}
	}

	// check for any errors on the cursor
	if commCur.Err() != nil {
		logger.Println("[LoadComments] ERROR: cursor error; ", commCur.Err())
		//msgMarsh, _ := json.Marshal(map[string]string{"message": "ERROR: cursor error; " + commCur.Err().Error()})
		response.JsonResponse(w, map[string]string{"message": "ERROR: cursor error; " + commCur.Err().Error()}, http.StatusInternalServerError)
		return
	}

	// call a function to write the HTTP response with the commentsList data
	logger.Println("[LoadComments] successfully loaded comments: ", commentsList)
	response.JsonResponse(w, commentsList, http.StatusOK)
}

// TODO: move to response.go after
/*func JsonResponse(w http.ResponseWriter, data interface{}, statCode int) {
	// marshal the given data to prepare it for the payload
	payload, marshErr := json.Marshal(data)
	if marshErr != nil {
		logger.Println("ERROR: failed to marshal the given data; ", marshErr.Error())
		return
	}

	// set the response headers
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statCode)

	_, writeErr := w.Write(payload)
	if writeErr != nil {
		logger.Println("ERROR: failed to send JSON response; ", writeErr)
	}
}*/


func main() {
	logger.Println("---------- start execution ----------")
	db.GetRegionalDatabase()
	logger.Println("---------- start router ----------")
	router.StartRouter()
}