module BackendService

require (
	bitbucket.org/emailgistics/emailgisticscommon v0.0.0-20190425172759-62f254baf6a6 // indirect
	contrib.go.opencensus.io/exporter/ocagent v0.4.4 // indirect
	github.com/Azure/azure-sdk-for-go v25.1.0+incompatible // indirect
	github.com/Azure/go-autorest v11.4.0+incompatible // indirect
	github.com/azure/go-autorest v11.4.0+incompatible // indirect
	github.com/coreos/go-oidc v2.0.0+incompatible // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/dimchansky/utfbom v1.1.0 // indirect
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/gorilla/csrf v1.5.1 // indirect
	github.com/gorilla/mux v1.7.0 // indirect
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/mongodb/mongo-go-driver v0.3.0 // indirect
	github.com/pquerna/cachecontrol v0.0.0-20180517163645-1555304b9b35 // indirect
	github.com/rs/cors v1.6.0 // indirect
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	go.mongodb.org/mongo-driver v1.0.1 // indirect
	go.opencensus.io v0.19.0 // indirect
	golang.org/x/crypto v0.0.0-20190211182817-74369b46fc67 // indirect
	gopkg.in/mgo.v2 v2.0.0-20180705113604-9856a29383ce // indirect
	gopkg.in/square/go-jose.v2 v2.3.1 // indirect
)
